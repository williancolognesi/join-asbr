<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Contato{
    
    private $id_contato;
    private $nome;
    private $dtNascimento;
    private $email;
    private $telefone;
    private $regiao;
    private $unidade;
    
    function getId_contato() {
        return $this->id_contato;
    }

    function getNome() {
        return $this->nome;
    }

    function getDtNascimento() {
        return $this->dtNascimento;
    }

    function getEmail() {
        return $this->email;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function setId_contato($id_contato) {
        $this->id_contato = $id_contato;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setDtNascimento($dtNascimento) {
        $this->dtNascimento = $dtNascimento;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }
    
    function getRegiao() {
        return $this->regiao;
    }

    function getUnidade() {
        return $this->unidade;
    }

    function setRegiao($regiao) {
        $this->regiao = $regiao;
    }

    function setUnidade($unidade) {
        $this->unidade = $unidade;
    }
    
}