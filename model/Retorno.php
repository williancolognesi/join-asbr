<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Retorno{
    
    private $codigo;
    private $erro;
    private $mensagem;
    
    public function __construct($codigo, $erro, $mensagem) {
        $this->codigo = $codigo;
        $this->erro = $erro;
        $this->mensagem = $mensagem;
    }
    function getCodigo() {
        return $this->codigo;
    }

    function getErro() {
        return $this->erro;
    }

    function getMensagem() {
        return $this->mensagem;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setErro($erro) {
        $this->erro = $erro;
    }

    function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    public function __toString() {
        $row = [
            "codigo" => $this->getCodigo(),
            "erro" => $this->getErro(),
            "mensagem" => $this->getMensagem()
        ];
        return json_encode($row);
    }
    
}