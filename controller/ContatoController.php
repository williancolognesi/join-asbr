<?php
require("../model/Contato.php");
require("../model/Retorno.php");
require("../helper/Connection.php");

if($_SERVER['REQUEST_METHOD'] == "POST"){
    
    if(isset($_POST['contato'])){
        switch($_POST['contato']){
            
            case "registrar":
                $contatoController = new ContatoController();
                
                $contato = new Contato();
                if(isset($_POST['id_contato'])){
                    if($_POST['id_contato'] != null){
                        $contatoNovo = $contatoController->getContato($_POST['id_contato']);
                        if($contatoNovo !== null){
                            $contato = $contatoNovo;
                        }
                    }
                }
                if(isset($_POST['nome'])){
                    $contato->setNome(addslashes($_POST['nome']));
                }
                if(isset($_POST['data_nascimento'])){
                    $contato->setDtNascimento(addslashes($_POST['data_nascimento']));
                }
                if(isset($_POST['email'])){
                    $contato->setEmail($_POST['email']);
                }
                if(isset($_POST['telefone'])){
                    $contato->setTelefone($_POST['telefone']);
                }
                if(isset($_POST['regiao'])){
                    $contato->setRegiao($_POST['regiao']);
                }
                if(isset($_POST['unidade'])){
                    $contato->setUnidade($_POST['unidade']);
                }
                
                if($contato->getId_contato() != null){
                    exit($contatoController->atualiza($contato));
                }else{
                    exit($contatoController->insere($contato));
                }
                break;
        }
    }
    
}
class ContatoController{
    
    /**
     * 
     * @param \Contato $contato
     */
    public function insere($contato){
        $con = Connection::getConnection();
        
        $insertContato = "INSERT INTO contato SET nome = ?, dtNascimento = ?, 
                            email = ?, telefone = ?, regiao = ?, unidade = ?";
        $pstmt = $con->prepare($insertContato);
        
        $nome = $contato->getNome();
        $dtNascimento = $contato->getDtNascimento();
        $email = $contato->getEmail();
        $tel = $contato->getTelefone();
        $regiao = $contato->getRegiao();
        $unidade = $contato->getUnidade();
        
        $pstmt->bind_param("ssssss", $nome, $dtNascimento, $email, $tel, $regiao, $unidade);
        if($pstmt->execute()){
            return new Retorno($pstmt->insert_id, false, "Contato registrado com sucesso!");
        }else{
            return new Retorno(1, true, "Não foi possível registar seu contato, por favor, tente novamente mais tarde!");
        }
    }
    
    /**
     * 
     * @param \Contato $contato
     */
    public function atualiza($contato){
        $con = Connection::getConnection();
        
        $insertContato = "UPDATE contato SET nome = ?, dtNascimento = ?, 
                            email = ?, telefone = ?, regiao = ?, unidade = ? 
                            WHERE id_contato = ?";
        $pstmt = $con->prepare($insertContato);
        
        $nome = $contato->getNome();
        $dtNascimento = $contato->getDtNascimento();
        $email = $contato->getEmail();
        $tel = $contato->getTelefone();
        $regiao = $contato->getRegiao();
        $unidade = $contato->getUnidade();
        
        $id = $contato->getId_contato();
        
        $pstmt->bind_param("ssssssi", $nome, $dtNascimento, $email, $tel, $regiao, $unidade, $id);
        if($pstmt->execute()){
            $con->close();
            return new Retorno(0, false, "Contato atualizado com sucesso!");
        }else{
            $con->close();
            return new Retorno(1, true, "Não foi possível registrar seu contato, por favor, tente novamente mais tarde!");
        }
    }
    
    public function getContato($contato_id){
        $con = Connection::getConnection();
        
        $selContato = "SELECT id_contato, nome, dtNascimento, email, telefone, 
                        regiao, unidade FROM contato WHERE id_contato = ? LIMIT 1";
        $pstmt = $con->prepare($selContato);
        $pstmt->bind_param("i", $contato_id);
        
        $pstmt->execute();
        if($pstmt->num_rows > 0){
            $results = $pstmt->get_result();
            $contatoBD = $results->fetch_assoc();
            
            $contato = new Contato();
            $contato->setId_contato($contatoBD['id_contato']);
            $contato->setNome($contatoBD['nome']);
            $contato->setDtNascimento($contatoBD['dtNascimento']);
            $contato->setEmail($contatoBD['email']);
            $contato->setTelefone($contatoBD['telefone']);
            $contato->setRegiao($contatoBD['regiao']);
            $contato->setUnidade($contatoBD['unidade']);
            
            $pstmt->free_result();
            $con->close();
            return $contato;
        }
        $con->close();
        return null;
    }
    
}