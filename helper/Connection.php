<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Connection{
    
    private static $local = "localhost";
    private static $login = "root";
    private static $senha = "willian@123";
    private static $banco = "join_asbr";
    
    public static function getConnection(){
        $conect = new mysqli(self::$local, self::$login, self::$senha, self::$banco);
        
        if(!$conect){
            throw new Exception('Erro ao conectar ao Banco de Dados!');
        }else{
            if($conect != null){
                $conect->query("SET NAMES 'utf8'");
                $conect->query('SET character_set_connection=utf8');
                $conect->query('SET character_set_client=utf8');
                $conect->query('SET character_set_results=utf8');
            }
            
            return $conect;
        }
    }
    
}